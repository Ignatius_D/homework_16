﻿#pragma warning(disable : 4996)
#include <iostream>
#include <ctime>

int main()
{
    //Get current day of month
    std::time_t t = std::time(nullptr);
    std::tm* local = std::localtime(&t);
    int currentDay = static_cast<int>(local->tm_mday);

    //Initialization array and enter elements to array
    const int N = 10;
    int arrayM[N][N];
    
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            arrayM[i][j] = i + j;
        }
    }

    //Print array
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            std::cout<<arrayM[i][j]<<" ";
        }
        std::cout << std::endl;
    }

    //Get sum of elements in array row, index of which is equal to 
    //remainder of division of current calendar number by N
    int sum=0;
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            if (i == (currentDay%N)) sum += arrayM[i][j];
        }
    }
    std::cout << "Result: " << sum << std::endl;
}

